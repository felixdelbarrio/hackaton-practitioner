import {LitElement, html} from 'lit-element'


class AppStats extends LitElement {


    static get properties(){
        return {
            items: {type: Array}
        }
    }

    constructor() {
        super()

        this.items = []
    }

    updated(changedProperties){
        console.log('updated en app-stats')

        if (changedProperties.has('items')) {
            console.log('Ha cambiado el valor de la propiedad people en persona-stats')

            let itemStats = this.gatherItemsArrayInfo(this.items)
            this.dispatchEvent(new CustomEvent('updated-stats', {
                detail: {
                    itemStats : itemStats
                }
            }))
        }
    }

    gatherItemsArrayInfo(items) {
        console.log('gatherItemArrayInfo')

        let itemStats = {}
        itemStats.numberOfItems = items.length

        return itemStats

    }
    
}

customElements.define('app-stats', AppStats)