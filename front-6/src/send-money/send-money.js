import {LitElement, html} from 'lit-element'

class SendMoney extends LitElement {

    static get properties(){
        return {
            importe: {type: Number},
        }
    }

    constructor() {
        super()

        this.importe = 0
    }
    
    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" 
                rel="stylesheet" 
                integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" 
                crossorigin="anonymous"
                > 
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" 
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" 
            crossorigin="anonymous"
            >
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" 
                integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" 
                crossorigin="anonymous"
                >
        </script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" 
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" 
            crossorigin="anonymous"
            >
        </script>             
        <div>
            <form>
                <div class="form-group">
                <label>Importe</label> 
                    <input type="number" 
                        @input="${this.importe}" 
                        .value=${this.importe}
                        class="form-control" 
                        placeholder="ID" 
                        id="importe"/ >
                    <br>
                    <br>
                <button  
                    @click="${this.cancelarEnvio}" 
                    class="btn btn-danger">
                        <strong>Cancelar</strong>
                </button>
                <button  
                    @click="${this.enviarDinero}" 
                    class="btn btn-primary">
                        <strong>Enviar</strong>
                    </button>
            </form>
            <br>
            <br>
        </div>
        <div 
            id="CardList"
            class="carousel slide"
            data-ride="carousel"
            >
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="../img/giphy.gif" class="d-block w-100" alt="1.">
                </div>
            </div>
        </div>
        `
    }
  
    cambiaImporte(e) {
        console.log('cambiaImporte')
        //console.log(`Actualizando id: ${e.target.value}`)
        this.importe = e.target.value   
    } 

    cancelarEnvio(e){
        
        console.log('go-back')
        //console.log(e)
        e.preventDefault()


        this.dispatchEvent(new CustomEvent('envio-cancelado'))
        
    }

    enviarDinero(e){
        console.log('store-item')
        console.log(this.editingItem)
        e.preventDefault()
        
        //console.log('StoreItem')

        let url = ""
        let data = {};
        let xhr = new XMLHttpRequest() 

        data.importe = this.importe

        let json = JSON.stringify(data);

        xhr.onload = ()=> {
            if (xhr.status === 200) {
              }
              else
              {

              }
        }

        //Pendiente de incluir uri
        //xhr.open('POST', url ,false)
        //xhr.setRequestHeader('Content-type','application/json; charset=utf-8');
        //xhr.send(json)

       
        this.dispatchEvent(new CustomEvent('envio-realizado', {}))
   
    }

}

customElements.define('send-money', SendMoney)