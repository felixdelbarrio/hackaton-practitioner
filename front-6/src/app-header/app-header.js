import {LitElement, html} from 'lit-element'


class AppHeader extends LitElement {


    static get properties(){
        return {

        }
    }

    constructor() {
        super()
    }
    
    render(){
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" 
                  rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" 
                  crossorigin="anonymous"
                  > 
            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" 
                    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" 
                    crossorigin="anonymous"
                    >
            </script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" 
                    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" 
                    crossorigin="anonymous"
                    >
            </script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" 
                    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" 
                    crossorigin="anonymous"
                    >
            </script>
            <div 
                id="CardList"
                class="carousel slide"
                data-ride="carousel"
                >
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="../img/tarjeta_bbva.gif" class="d-block w-100" alt="1.">
                    </div>
                    <div class="carousel-item">
                        <img src="../img/giphy.gif" class="d-block w-100" alt="2.">
                    </div>
                </div>
                <a class="carousel-control-prev" 
                    href="#CardList" 
                    role="button" 
                    data-slide="prev"
                    >
                    <span 
                        class="carousel-control-prev-icon" 
                        aria-hidden="true">
                    </span>
                    <span 
                        class="sr-only">
                    </span>
                </a>
                <a class="carousel-control-next" 
                    href="#CardList" 
                    role="button" 
                    data-slide="next"
                    >
                    <span 
                        class="carousel-control-next-icon" 
                        aria-hidden="true"
                    >
                    </span>
                    <span 
                        class="sr-only">
                     </span>
                 </a>   
            </div>
        `
    }
}

customElements.define('app-header', AppHeader)