import {LitElement, html} from 'lit-element'

class ItemForm extends LitElement {

    static get properties(){
        return {
            targetItem: {type: Object},
            editingItem: {type : Boolean}
        }
    }

    constructor() {
        super()

        this.resetFormData()
        this.editingItem = false
    }
    
    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" 
                rel="stylesheet" 
                integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" 
                crossorigin="anonymous"
                > 
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" 
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" 
            crossorigin="anonymous"
            >
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" 
                integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" 
                crossorigin="anonymous"
                >
        </script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" 
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" 
            crossorigin="anonymous"
            >
        </script>             
        <div>
            <form>
                <div class="form-group">
                <label>Id</label> 
                    <input type="number" 
                        @input="${this.identifier}" 
                        .value=${this.targetItem.id}
                        class="form-control" 
                        placeholder="ID" 
                        id="id"/ >   
                <label>Nombre </label> 
                <input type="text" 
                    @input="${this.updateName}" 
                    .value="${this.targetItem.name}" 
                    class="form-control" 
                    placeholder="Nombre " 
                    id="fname"/ >

                <label>Apellido </label> 
                <input type="text" 
                    @input="${this.updateSurname}" 
                    .value="${this.targetItem.surname}" 
                    class="form-control" 
                    placeholder="Apellido" 
                    id="fsurname"/ >

                <label>Teléfono</label>
                <input type="text" 
                    @input="${this.updatePhone}" 
                    .value="${this.targetItem.phone}" 
                    class="form-control" 
                    placeholder="Teléfono" 
                    id="fphone">
                    </textarea>
                </div>
                <button  
                    @click="${this.goBack}" 
                    class="btn-primary">
                        <strong>Atrás</strong>
                </button>
                <button  
                    @click="${this.storeItem}" 
                    class="btn-success">
                        <strong>Guardar</strong>
                    </button>
            </form>
        </div>
        `
    }
  
    identifier(e) {
        console.log('identifier')
        //console.log(`Actualizando id: ${e.target.value}`)
        this.targetItem.id = e.target.value   
    } 
    updateName(e){
        console.log('updatename')
        //console.log(`Actualizando name: ${e.target.value}`)
        this.targetItem.name = e.target.value
    }
    updateSurname(e){
        console.log('updateSurname')
        //console.log(`Actualizando Surname: ${e.target.value}`)
        this.targetItem.surname = e.target.value
    }
    updatePhone(e){
        console.log('updatePhone')
        //console.log(`Actualizando Phone: ${e.target.value}`)
        this.targetItem.phone = e.target.value
    }   

    goBack(e){
        
        console.log('go-back')
        //console.log(e)
        e.preventDefault()

        this.resetFormData()
        this.dispatchEvent(new CustomEvent('item-form-close'))
        
    }

    storeItem(e){
        console.log('store-item')
        console.log(this.editingItem)
        e.preventDefault()
        
        let uId = ''

        if (this.editingItem === true) {
            uId = this.targetItem.id

        }else {
            uId = (new Date).toString(36)
        }

        console.log(`la propiedad editingItem: ${this.editingItem}`)
        console.log(`la propiedad id: ${uId}`)
        console.log(`la propiedad name: ${this.targetItem.name}`)
       
        this.dispatchEvent(new CustomEvent('item-form-store', {
            detail: {
                item : {
                        id: this.targetItem.id,
                        name: this.targetItem.name,
                        surname: this.targetItem.surname,
                        phone: this.targetItem.phone
                },
                editingItem: this.editingItem
            }
        }))

        this.resetFormData()
        
    }

    resetFormData() {
        
        //console.log('resetFormData')

        this.targetItem = {}
        this.targetItem.id = "" 
        this.targetItem.name = ""
        this.targetItem.surname = "" 
        this.targetItem.phone = "" 
        this.editingItem = false

    }


}

customElements.define('item-form', ItemForm)