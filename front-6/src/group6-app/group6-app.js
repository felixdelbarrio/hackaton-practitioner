import {LitElement, html} from 'lit-element'

import '../app-header/app-header.js'
import '../app-main/app-main.js'
import '../app-footer/app-footer.js'
import '../app-sidebar/app-sidebar.js'
import '../app-stats/app-stats.js'



class Group6App extends LitElement {


    static get properties(){
        return {
            items: {type: Array}

        }
    }

    constructor() {
        super()
        this.items = []
    }
    
    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" 
                rel="stylesheet" 
                integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" 
                crossorigin="anonymous"
                > 
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" 
                integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" 
                crossorigin="anonymous"
                >
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" 
                integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" 
                crossorigin="anonymous"
                >
        </script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" 
                integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" 
                crossorigin="anonymous"
                >
        </script>         
        <app-header></app-header>
            <div class="row">
                <app-sidebar 
                    @new-item="${this.newItem}" 
                    class="col-2"
                    >
                </app-sidebar>
                <app-main 
                    @updated-items ="${this.updatedItems}"
                    class="col-10">
                </app-main>
            </div>
            <app-footer></app-footer>
            <app-stats
                @updated-stats='${this.itemUpdated}'>
            </app-stats>     
        `
    }

    updated(changedProperties) {
        console.log('updated')

        if (changedProperties.has('items')){
            console.log(`Ha cambiado el valor de items: ${this.items}`)
            this.shadowRoot.querySelector('app-stats').items = this.items

        }
    }

    updatedItems(e){
        console.log('updatedItems group6')
        this.items = e.detail.items
    }
    
    newItem(e) {
        //console.log('newitem en group6-app')
        //console.log(e)

        this.shadowRoot.querySelector('app-main').showItemForm = true
    }
    itemUpdated(e){
        console.log('itemUpdated')
        console.log(e.detail)

        this.shadowRoot.querySelector('app-sidebar').itemStats = e.detail.itemStats
    }

    
}

customElements.define('group6-app', Group6App)