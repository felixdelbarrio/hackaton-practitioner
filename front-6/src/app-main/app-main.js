import {LitElement, html} from 'lit-element'
import '../app-items/app-items.js'
import '../item-form/item-form.js'
import '../send-money/send-money.js'


class AppMain extends LitElement {

    static get properties(){
        return {
            items : {type: Array},
            showItemForm : {type: Boolean},
            showSendMoney : {type: Boolean},
            itemSelected : {type: Object}
        }
    }

    constructor() {
        super()
       
        this.items = []
        this.getData()
        this.itemSelected = {}
        this.showItemForm = false
        this.showSendMoney = false
    }
    
    render(){
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" 
                  rel="stylesheet" 
                  integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" 
                  crossorigin="anonymous"
                  > 
            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" 
                    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" 
                    crossorigin="anonymous"
                    >
            </script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" 
                    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" 
                    crossorigin="anonymous"
                    >
            </script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" 
                    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" 
                    crossorigin="anonymous"
                    >
            </script>
            <h2 class="text-center">
                Contactos
            </h2>
            <div 
                id="itemList"
                class="carousel slide"
                data-ride="carousel"
                >
                <div>
                    ${this.items.map(
                        item => html`
                                    <div class="carousel-iteme">
                                        <app-items 
                                            id =${item.id}
                                            name ="${item.name}"
                                            phone="${item.phone}"
                                            surname="${item.surname}"
                                            @delete-item="${this.deleteItem}"
                                            @update-item="${this.getItem}"  
                                            @send-item="${this.sendItem}" 
                                            >
                                        </app-items>
                                    </div>
                                    `
                                    )
                    }
                </div> 
                <a class="carousel-control-prev" 
                    href="#itemList" 
                    role="button" 
                    data-slide="prev"
                    >
                    <span 
                        class="carousel-control-prev-icon" 
                        aria-hidden="true">
                    </span>
                    <span 
                        class="sr-only">
                        Previous
                    </span>
                </a>
                <a class="carousel-control-next" 
                    href="#itemList" 
                    role="button" 
                    data-slide="next"
                    >
                    <span 
                        class="carousel-control-next-icon" 
                        aria-hidden="true"
                    >
                    </span>
                    <span 
                        class="sr-only">
                        Next
                    </span>
                </a>   
            </div>
            <div>
                <item-form  
                    id="itemForm" 
                    class="row d-none "
                    @item-form-close="${this.itemFormClose}" 
                    @item-form-store="${this.storeItem}"                             
                >
                </item-form>
                <send-money  
                    id="sendMoney" 
                    class="row d-none "
                    @envio-cancelado="${this.cancelSendMoney}" 
                    @envio-realizado="${this.acceptSendMoney}"       
                >
                </send-money>
            </div>     
        `
    }
    
    updated(changedProperties){
        
        console.log(`updated: ${changedProperties}`)

        if (changedProperties.has("showItemForm")){
            console.log('entra a showItemForm')
            this.showItemForm === true ? this.showItemFormData() : this.showItemList()
        }

        if (changedProperties.has("showSendMoney")){
            console.log('entra a showSendMoney')
            this.showSendMoney === true ? this.showSendAction() : this.showItemList()
        }

        
        
        if(changedProperties.has("itemSelected") && this.itemSelected && Object.keys(this.itemSelected).length){
            //console.log("entra a item selected")    
            this.shadowRoot.getElementById('itemForm').editingItem = true
        
        }

        if (changedProperties.has("items")){
            console.log('updated items',this.items)
            this.dispatchEvent(new CustomEvent('updated-items', {

                'detail': {
                    items: this.items
                }
            }))
        }


    }
    
    itemFormClose(){
        //console.log('itemFormClose')
        //console.log('Se ha cerrado el formulario del Item')
        this.showItemForm = false
    } 

    showItemFormData() {
        //console.log('show showItemFormData en app-main')
        //console.log('mostrar formulario de item')

        
        this.shadowRoot.getElementById('itemForm').classList.remove('d-none')
        this.shadowRoot.getElementById('itemList').classList.add('d-none')

       

    }
    showItemList() {
        //console.log('showItemList en app-main')
        //console.log('mostrar listado de items')
        
        this.getData()

        this.shadowRoot.getElementById('itemForm').classList.add('d-none')
        this.shadowRoot.getElementById('sendMoney').classList.add('d-none')
        this.shadowRoot.getElementById('itemList').classList.remove('d-none')

    }

    showSendAction() {
        console.log('showSendMoney en app-main')

        this.shadowRoot.getElementById('itemList').classList.add('d-none')
        this.shadowRoot.getElementById('sendMoney').classList.remove('d-none')

    }


     getData() {
        console.log('getData')
        //console.log('Obteniendo información de la API')

        let xhr = new XMLHttpRequest()

        xhr.onload = ()=> {
            if (xhr.status === 200) {
                //console.log("Peticion se ha completado correctamente")

                let APIResponse = JSON.parse(xhr.responseText)
                
                console.log(APIResponse)
                
                this.items = APIResponse

                this.requestUpdate

            }
        }

        xhr.open('GET', 'http://localhost:8080/v1/users')
        
        xhr.send()

        //console.log('Fin de getData')

    }
    getItem(e) {
        //console.log('getItem')
        console.log(e)
        let xhr = new XMLHttpRequest()

        xhr.onload = ()=> {
            
            if (xhr.status === 200) {
                let APIResponse = JSON.parse(xhr.responseText)
                console.log(APIResponse)

                let itemToShow = {}

                itemToShow.id = APIResponse.id 
                itemToShow.name = APIResponse.name 
                itemToShow.surname = APIResponse.surname
                itemToShow.phone  = APIResponse.phone


                console.log(`ìtemToShow: ${itemToShow}`)
                this.shadowRoot.getElementById('itemForm').targetItem = itemToShow
                this.shadowRoot.getElementById('itemForm').editingItem = true

                this.showItemForm = true

            }
        }

        console.log(`Obteniendo información del id: ${e.detail.id}`)
        //xhr.open('GET', `http://localhost:8080/v1/users/${e.detail.id}`)
        xhr.open('GET', 'http://localhost:8080/v1/users/' + e.detail.id)
        xhr.send()

        //console.log('Fin de getItem')

    }
    storeItem(e){
        //console.log('StoreItem')
        let verb = ''
        let uId = ''
        let urI = "http://localhost:8080/v1/users/"

        let data = {};

        if (e.detail.editingItem === true) {
            verb = 'PUT'
            urI = urI + e.detail.item.id

        } else {
            verb = 'POST'
            uId= new Date()
            data.id = e.detail.item.id
        }

        //console.log(`Trabajando sobre id: ${this.uId}`)
        
        
        let xhr = new XMLHttpRequest() 

        
        data.name = e.detail.item.name
        data.surname  = e.detail.item.surname
        data.phone = e.detail.item.phone

        let json = JSON.stringify(data);

        xhr.onload = ()=> {
            if (xhr.status === 200) {
              }
              else
              {

              }
        }

        xhr.open(verb, urI,false)
        xhr.setRequestHeader('Content-type','application/json; charset=utf-8');
        
        xhr.send(json)

        

        //console.log('Fin de StoreItem')

        this.showItemForm = false
    }
    deleteItem(e) {
        //console.log('deleteItem')

        let xhr = new XMLHttpRequest()

        xhr.onload = ()=> {
            if (xhr.status === 200) {

                this.getData()
            }
        }

        //console.log('valor del id ' + id)
        xhr.open('DELETE', "http://localhost:8080/v1/users/" + e.detail.id)
        //Para un POST en el hr.send('BODY REQUEST')
        xhr.send()

        //console.log('Fin de deleteItem')
        

    }
    sendItem(e) {
        console.log('sendItem')

        this.showSendMoney = true
    }
    cancelSendMoney(e){
        console.log('cancelSendMoney')
        this.showSendMoney = false
    }
    acceptSendMoney(e){
        console.log('acceptSendMoney')
        this.showSendMoney = false
    }

}

customElements.define('app-main', AppMain)