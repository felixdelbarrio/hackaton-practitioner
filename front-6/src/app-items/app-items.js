import {LitElement, html} from 'lit-element'


class AppItems extends LitElement {


    static get properties(){
        return {
            id: {type: Number},
            name: {type: String},
            surname: {type: String},
            phone: {type: String}
            
        }
    }

    constructor() {
        super()
    }
    
    render(){
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" 
                rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" 
                crossorigin="anonymous"
                > 
            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" 
                integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" 
                crossorigin="anonymous"
                >
            </script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" 
                integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" 
                crossorigin="anonymous"
                >
            </script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" 
                integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" 
                crossorigin="anonymous"
                >
            </script>
            <div class="card h-100">
                <div class="card-body">
                    <h5 class="card-title">${this.name} ${this.surname}</h5>
                    <p class="card-text">${this.phone}</p>
                </div>
                <div class="card-footer">
                    <button 
                        @click="${this.deleteItem}" 
                        class="btn btn-danger col-2 offset-1"
                        >
                            <strong>x</strong>
                    </button>
                    <button 
                        @click="${this.updateItem}" 
                        class="btn btn-info col-3 offset-1"
                        >
                            <strong>info</strong>
                    </button>
                    <button 
                        @click="${this.sendToItem}" 
                        class="btn btn-primary col-3 offset-1"
                        >
                            <strong>Envia</strong>
                    </button>
                </div>
            </div>
        `
    }

    deleteItem(e){
        //console.log('deleteItem en app-item.js')
        //console.log(`Se va a borrar el item ${this.id}`)
        
        this.dispatchEvent(
            new CustomEvent(
                'delete-item',
                {
                    'detail': {
                        'id': this.id
                    }
                }
            )
        )
    }
    updateItem(e) {
        //console.log('updateItem en app-item.js')
        //console.log(`Se va a actualizar item ${this.id}`)

        this.dispatchEvent(
            new CustomEvent(
                'update-item',
                {
                    'detail': {
                        'id': this.id
                    }
                }
            )
        )
    }
    sendToItem(e){
        //console.log('sendToItem en app-item.js')
        //console.log(`Se va a realizar un envio al item ${this.id}`)

        this.dispatchEvent(
            new CustomEvent(
                'send-item',
                {
                    'detail': {
                        'id': this.id
                    }
                }
            )
        )
    }    
}

customElements.define('app-items', AppItems)